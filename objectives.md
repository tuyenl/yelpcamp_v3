# Add Mongoose
    * Install and configure Mongoose
    * Setup campground model
    * Use campground model inside of our routes

# Show Page
    * Review the RESTful routes
    * Add description to our campground model
    * Show db.collection.drop() // example: db.campgrounds.drop() removes collection from mongo db
    * Add a show route/template

# Refactor Mongoose Code
* Create a models directory
* User module.exports
* Require everything correctly 

# Add Seeds File
* Add a seed.js file
* Run the seeds file every time the server starts'


========================================================================
# Current Routes

Name            Path                                  Http Verb           Purpose
=============================================================================================================
INDEX          /campgrounds                             GET              Show all campgrounds
NEW            /campgrounds/new                         GET              Display form to create new campgrounds
CREATE         /campgrounds                             POST             Add new campground to db
SHOW           /campgrounds/:id                         GET              Display specific campground/more info on one particular campground
NEW            /campgrounds/:id/comments/new            GET              Display form to create new comment for specific campground
CREATE         /campgrounds/:id/comments                POST             Store new comment in db for specific campground 


## Comment and Create Routes
* Add the comment new and create routes
* Add the new comment form

## Style the show page
* Add sidebar to show page
* Display comments nicely

## Finish Styiling Show Page
* Add public directory
* Add custom stylesheet

## Authentication Add user model part1
* Install package for authentication
* Define user model 

## Authenication part 2 - Register route and creation
* Configure passport
* Add register routes
* Add register template

## Authentication part 3 - Login Route
* Add login routes
* Add login template

## Authentication part 4 - Logout/Navbar
* Add logout route
* Prevent users from commenting when not signed in
* Add links to navbar
* Show/hide auth links correctly 

## Authentication part 5 - Show/Hide links
* Show/Hide auth links in navbar correctly 

## Refactor the routes
* Use express router to reorganize all routes

## Associate User with Comments
* Restructed the comment schema to reference user
* Save authors name to comment


## Associate User with Campgrounds
* Prevent an unauthenticated user from creating a campground
* Save username + id to newly created campground


## EDITING CAMPGROUNDS
* Add Method-Override
* Add Edit Route for Campgrounds
* Add Link To Edit Page
* Add Update Route
* Fix $set problem

## Deleting Campgrounds
* Add Destroy Route
* Add Delete Button

## Authorization
* User can only edit his/her campgrounds
* User can only delete his/her campgrounds
* Hide/show edit/delete buttons
* Notes
    - Mongoose data is retrieved as an object, if you console.log the data it may appear to be a string 
    but you cannot use === or == to compare the user.id with mongoose object.
    To compare you can you use a mongoose funciton .equal to find if both the user.id and mongoose object are equal.

    - currentUser is passed to all views template by using 
    app.use((req,res,next)=>{
    res.locals.currentUser = req.user;
    next();

    in the app.js section
})

## Editing Comments 
* Add edit route for comments
 - /campgrounds/:id/comments/comment_id/edit
* Add edit button
* Add update route

## Deleting Comments
* Add Destroy route
- Campground Destroy Route: /campgrounds/:id
- Comment Destroy Route: /campgrounds/:id/comments/:comment_id
* Add Delete Button

## Authorization on comment
* User can only edit his/her comment
* User can only delete his/her comment
* Hide/show edit and delete buttons
* Refactor middleware

## Authorization Part 2: Comments
* User can only edit his/her comments
* User can only delete his/her comments
* Hide/show edit delete buttons
* Refactor middleware

## Adding in Flash Messages
 * Demo working version
 * Install and configure connect-flash
 * Add bootstrap alerts to header