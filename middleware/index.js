var Campground = require('../models/campground');
var Comment    = require('../models/comment');

var middlewareObj = {};

middlewareObj.checkCampgroundOwnership = function(req, res, next){
    //check if user is loggedIn
    if(req.isAuthenticated()){
        Campground.findById(req.params.id, (err, foundCampground)=>{
            if(err){
                req.flash("error", "Campground not found");
                res.redirect("back");
            }
            //Checks if campground belongs to user logged in
            else if(foundCampground.author.id.equals(req.user._id)){
               next();
            }
            else{
                req.flash("error", "you dont have permission to do that");
                res.redirect("back");
            }
        })
    }
    else {
        req.flash("error", "You need to log in to do that");
        res.redirect("back"); // redirect user back to where they came from 
    }
}

middlewareObj.checkCommentOwnership = function (req, res, next){
    //check if user is loggedIn
    if(req.isAuthenticated()){
        Comment.findById(req.params.comment_id, (err, foundComment)=>{
            if(err){
                req.flash("error", "Comment not found");
                res.redirect("back");
            }
            //Checks if campground belongs to user logged in
            else if(foundComment.author.id.equals(req.user._id)){
               next();
            }
            else{
                req.flash("error", "you dont have permission to do that");
                res.redirect("back");
            }
        })
    }
    else {
        req.flash("error", "You need to log in to do that");
        res.redirect("back"); // redirect user back to where they came from 
    }
}

middlewareObj.isLoggedIn = function(req,res,next){
    if(req.isAuthenticated()){
        return next();
    }
    req.flash("error", "You need to login");
    res.redirect("/login");
}


module.exports = middlewareObj;