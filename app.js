var express        = require('express'),
    app            = express(), 
    bodyParser     = require('body-parser'),
    mongoose       = require('mongoose');
    Campground     = require("./models/campground"),
    Comment        = require("./models/comment"),
    flash          = require("connect-flash"),
    seedDB         = require("./seeds"),
    passport       = require('passport'),
    methodOverride = require('method-override'),
    LocalStrategy  = require('passport-local'),
    User           = require("./models/user");


//REQUIRE ROUTES
var commentRoutes    = require("./routes/comments"),
    campgroundRoutes = require("./routes/campgrounds"),
    indexRoutes       = require("./routes/index");
 
mongoose.connect("mongodb://localhost:27017/yelp_camp_v3",{ useNewUrlParser:true, useUnifiedTopology: true });
app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public")); // to serve the public folder then you must link the css file in the header
// seedDB(); /seeding database  // id changes therefore will not retrieve the same page everytime we restart.
// to fix you must drop the database and recreate again.
app.use(flash());
app.use(methodOverride("_method"));
//PASSPORT CONFIGURATION
app.use(require('express-session')({
    secret: "I am the best programmer in the world",
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// Passes current users to all routes 
//req.user will container current username and id if signed else will be undefined
app.use((req,res,next)=>{
    res.locals.currentUser = req.user;
    res.locals.error = req.flash("error");
    res.locals.success = req.flash("success");
    next();
})


app.use("/campgrounds/:id/comments",commentRoutes);
app.use("/campgrounds", campgroundRoutes);
app.use(indexRoutes);

app.listen(3000, () =>{
    console.log("Server has started for yelpcamp on port 3000");
})