var express = require('express');
var router  = express.Router({mergeParams:true}); // for comments page to access the id param this is mergeParams is needed
var Campground = require('../models/campground');
var Comment  = require('../models/comment');
var middlewareObj = require('../middleware');



//========================================
//COMMENT ROUTES
//========================================

//COMMENTS NEW
router.get("/new", middlewareObj.isLoggedIn, function(req,res){

    Campground.findById(req.params.id, function(err, foundcampground){
        if(err){
            console.log(err);
        }
        else{
            res.render("comments/new", {campground: foundcampground});
        }
    })   
})

//COMMENTS CREATE
router.post("/", middlewareObj.isLoggedIn, function(req, res){

    Campground.findById(req.params.id, function(err, campground){
        if(err){
            console.log(err);
            res.redirect("/campgrounds");
        }
        else{
            Comment.create(req.body.comment, function(err, comment){
                if(err){
                    req.flash("error", "Something went wrong");
                    console.log(err);
                }
                else{
                    // add username and id to comment
                    comment.author.id = req.user._id;
                    comment.author.username = req.user.username;
                    //save comment
                    comment.save();
                    campground.comments.push(comment);
                    campground.save();
                    req.flash("success", "Succesfully added comment ")
                    res.redirect("/campgrounds/"+campground._id);
                }
            });
        
        }
    })

})
//COMMENT EDIT ROUTE
router.get("/:comment_id/edit", middlewareObj.checkCommentOwnership,(req, res)=>{
    Comment.findById(req.params.comment_id, (err, foundComment)=>{
        if(err){
            res.redirect("back");
        }
        else{
            res.render("comments/edit", {campground_id: req.params.id, comment: foundComment});
        }
    })
   
})

//COMMENT UPDATE ROUTE
router.put("/:comment_id", middlewareObj.checkCommentOwnership, (req, res)=>{
    Comment.findByIdAndUpdate(req.params.comment_id,req.body.comment,(err, updatedComment)=>{
        if(err){
            res.redirect("back");
        }
        else{
            res.redirect("/campgrounds/" + req.params.id);
        }
    })
})

//COMMENT DELETE ROUTE

router.delete("/:comment_id", middlewareObj.checkCommentOwnership,(req, res)=>{
    Comment.findByIdAndRemove(req.params.comment_id, (err, deletedComment)=>{
        if(err){
            res.redirect("back");
        }
        else{
            req.flash("success", "Comment succesfully deleted");
            res.redirect("/campgrounds/"+req.params.id);
        }
    })
})

module.exports = router;