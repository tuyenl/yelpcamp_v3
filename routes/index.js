var express = require('express');
var router  = express.Router();
var passport = require('passport');
var User = require("../models/user");


// AUTHENTICATION ROUTE
//Root Route
router.get("/", (req,res) => {
    res.render("landing");
})

//Register Form
router.get("/register", (req, res)=>{
    res.render("register");
})

//handle form logic
router.post("/register", (req,res)=>{

    var newUser = new User({username: req.body.username});
    User.register(newUser, req.body.password, (err, user)=>{
        if(err){
            req.flash("error", err.message);
            return res.render("register");
        }
    
        passport.authenticate("local")(req,res,function(){  
            req.flash("success", "Welcome to Yelpcamp " + user.username);
            res.redirect("/campgrounds");
        });
    })

});

//Show Login Form
router.get("/login", (req,res)=>{
    res.render("login")
})

// handles login logic
// app.post("/login", middleware, callback)
router.post("/login", passport.authenticate("local",{
    successRedirect: "/campgrounds",
    failureRedirect: "/login"
}), (req, res)=>{
    
});


//Logout route
router.get("/logout",(req,res)=>{
    req.logout();
    req.flash("success", "You have logged out");
    res.redirect("/campgrounds");
})



module.exports = router;