var express = require('express');
var router = express.Router();
var middlewareObj = require('../middleware');



//INDEX -- show all campgrounds
router.get("/", (req, res) =>{
    //res.render("campgrounds",{campgrounds: campgrounds})
    //Retrieve all campground from db
    Campground.find({}, (err, allcampgrounds)=>{
        if(err){
            console.log(err);
        }
        else{
            res.render("campgrounds/index", {campgrounds: allcampgrounds});
        }
    })
})

//CREATE -- add new campground to database
router.post("/",middlewareObj.isLoggedIn, (req,res) => {
    var name = req.body.name;
    var price = req.body.price;
    var image = req.body.image;
    var description = req.body.description;
    var author = {
        id: req.user._id,
        username: req.user.username
    }
    var newCampgrounds = {name: name, price: price, image: image,description: description, author: author};
    Campground.create(newCampgrounds,(err, newlyCreated)=>{
        if(err){
            console.log(err);
        }
        else{
            res.redirect("/campgrounds");
        }
    })
})



//NEW -- Show form to create new campground
router.get("/new", middlewareObj.isLoggedIn, (req, res) =>{
    res.render("campgrounds/new");
})


//SHOW -- show more info on one campground
router.get("/:id", (req,res) =>{
        Campground.findById(req.params.id).populate("comments").exec(function(err, foundCampground){
            if(err){
                console.log(err);
            }
            else{
                res.render("campgrounds/show", {campground: foundCampground})
            }
        })
    });


// EDIT CAMPGROUND ROUTE
router.get("/:id/edit", middlewareObj.checkCampgroundOwnership, (req, res)=>{
    Campground.findById(req.params.id, (err, foundCampground)=>{
        res.render("campgrounds/edit", {campground: foundCampground});
    });

});

// UPDATE CAMPGROUND ROUTE
router.put("/:id", middlewareObj.checkCampgroundOwnership, (req, res)=>{
    Campground.findByIdAndUpdate(req.params.id, req.body.campground, (err, updatedCampground)=>{
        if(err){
            res.redirect("/campgrounds")
        }
        else{
            res.redirect("/campgrounds/" + req.params.id);
        }
    })

})

//DESTROY CAMPGROUND ROUTE
router.delete("/:id", middlewareObj.checkCampgroundOwnership, (req,res)=>{
    Campground.findByIdAndRemove(req.params.id, (err)=>{
        if(err){
            res.redirect("/campgrounds");
        }
        else{
            req.flash("success","Campground succesfully deleted")
            res.redirect("/campgrounds");
        }
    })
})

module.exports = router;